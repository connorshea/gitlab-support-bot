$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'supportbot'
require 'web'

Thread.new do
  begin
    SupportBot::Bot.run
  rescue StandardError => e
    STDERR.puts "ERROR: #{e}"
    STDERR.puts e.backtrace
    raise e
  end
end

run SupportBot::Web

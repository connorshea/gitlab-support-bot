require 'zendesk_api'
require 'json'

module ZendeskHelper
  require_relative '../services/zendesk'
  def self.ticket_details
    zd_client.views.find(id: TIER1_VIEW_ID).rows.each do |row|
      ticket = zd_client.tickets.find(id: row.ticket.id)
      puts ticket.subject
      puts ticket.created_at
    end
  end

  def self.tickets_from_views
    zendesk_views = { tier_1: 149_450_768, tier_2: 149_937_247, tier_3: 159_699_368, twitter: 805_033_38 }
    output_hash = {}
    zendesk_views.each do |view, id|
      api_response = zd_client.view_counts(ids: id, path: 'views/count_many', reload: true).fetch.first
      output_hash[view] = api_response['value']
    end
    output_hash
  end
end

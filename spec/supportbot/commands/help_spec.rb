require 'spec_helper'

describe SupportBot::Commands::Help do
  def app
    SupportBot::Bot.instance
  end
  it 'help' do
    expect(message: 'supportbot help').to respond_with_slack_message('See https://gitlab.com/MrChrisW/gitlab-support-bot')
  end
end

require 'spec_helper'

describe SupportBot::Commands::Tickets do
  def app
    SupportBot::Bot.instance
  end
  it 'gets new tickets from views' do
    response = "Ticket Statistics - New\nTier 1: *4*\nTier 2: *4*\nTier 3: *4*\nTwitter: *4*"
    expect(message: 'supportbot tickets', channel: 'channel').to respond_with_slack_message(response)
  end
end

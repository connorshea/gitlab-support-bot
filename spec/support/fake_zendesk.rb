require 'sinatra/base'

class FakeZendesk < Sinatra::Base
  get '/api/v2/views/count_many' do
    json_response 200, 'count_many.json'
  end

  private

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/' + file_name, 'rb').read
  end
end

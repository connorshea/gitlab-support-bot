module SupportBot
  module Commands
    class Unknown < SlackRubyBot::Commands::Base
      match(/^(?<bot>\S*)[\s]*(?<expression>.*)$/)

      def self.call(_client, _data, _match)
      end
    end

    class About < SlackRubyBot::Commands::Base
      command 'about', 'hi', 'help' do |_client, _data, _match|
      end
    end
  end
end

module SupportBot
  module Commands
    class Help < SlackRubyBot::Commands::Base
      command 'help' do |client, data, _match|
        client.say(channel: data.channel, text: 'See https://gitlab.com/MrChrisW/gitlab-support-bot')
      end
    end
  end
end

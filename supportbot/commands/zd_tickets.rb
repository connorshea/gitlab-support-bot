require 'helpers/zendesk_helper'

module SupportBot
  module Commands
    class Tickets < SlackRubyBot::Commands::Base
      include ZendeskHelper
      command 'tickets'
      def self.call(client, data, _match) # rubocop:disable Metrics/MethodLength
        tickets = ZendeskHelper.tickets_from_views
        if tickets
          response = "Ticket Statistics - New\n"\
                     "Tier 1: *#{tickets[:tier_1]}*\n"\
                     "Tier 2: *#{tickets[:tier_2]}*\n"\
                     "Tier 3: *#{tickets[:tier_3]}*\n"\
                     "Twitter: *#{tickets[:twitter]}*"
          client.say(channel: data.channel, text: response)
        else
          client.say(channel: data.channel, text: 'Failed getting tickets')
        end
      rescue StandardError => e
        client.say(channel: data.channel, text: "Sorry, #{e.message}.")
      end
    end
  end
end

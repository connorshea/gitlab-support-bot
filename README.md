# Support Bot [![Build status](https://gitlab.com/MrChrisW/gitlab-support-bot/badges/master/build.svg)](https://gitlab.com/MrChrisW/gitlab-support-bot/commits/master)

### Installation

```
git clone https://gitlab.com/MrChrisW/gitlab-support-bot.git
cd gitlab-support-bot
bundle install
```

#### Export API tokens

```
export SLACK_API_TOKEN=LONGTOKEN # Slack API token
export ZD_TOKEN=LONGTOKEN # Zendesk API token
```

## Commands

#### Report ticket statistics

```
supportbot tickets
```

**Output**

```
Ticket Statistics - New
Tier 1: 4
Tier 2: 2
Tier 3: 4
Twitter: 4
```